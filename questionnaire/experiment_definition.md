# BabiaXR, time evolution JetUML CodeCity experiment

Welcome to our experiment! In this experiment, we want to assess whether it is better to navigate in the evolution of a software system project using on-screen technologies, or using Virtual Reality (VR) headsets with the CodeCity visualization.

If you have any questions during the experiment, please ask the supervisor.


## Software System Information 

The project that will be analyzed in this experiment is the JetUML project ([https://www.jetuml.org/](https://www.jetuml.org/)), JetUML is a lightweight desktop application for interactively creating and editing diagrams in the Unified Modeling Language. JetUML supports the sketching of software design ideas with a minimum of fuss. Diagrams can be saved in JSON, exported to popular image formats, and copied to the system clipboard for integration with other tools. It supports class diagrams, sequence diagrams, state diagrams, object diagrams, and use case diagrams.


![alt_text](images/image1.png "image_tooltip")


JetUML is hosted on GitHub ([https://github.com/prmr/JetUML](https://github.com/prmr/JetUML)) and in this experiment, we will focus on different stages of the project, corresponding to its releases.



* About releases: [https://docs.github.com/en/repositories/releasing-projects-on-github/about-releases](https://docs.github.com/en/repositories/releasing-projects-on-github/about-releases)

Each release has information about the specific commit and date. You were asked to do the experiment using either BabiaXR or an on-screen method (GitHub as an entrypoint). Please, go to the section corresponding to your environment.


## GitHub page of JetUML (On-Screen)

You are going to visit the repository of JetUML project ([https://github.com/prmr/JetUML](https://github.com/prmr/JetUML)). Specifically, you are going to be asked to perform program comprehension tasks related to the releases of the project. Please, take your time to get used to them:



* JetUML releases: [https://github.com/prmr/JetUML/releases](https://github.com/prmr/JetUML/releases)

Notice that each release has information about the specific commit and its date, in addition to the name and tag.

If you want, you can clone the repository and navigate into the releases using whatever you desire, on the IDE, the command line, other UI, etc. There is no limit number of applications.

When you are ready to start, let the supervisor know.


## Pre-Experiment Requisites

For this experiment, we will record the answers via voice responding as well as screen recording for checking that the answers provided match with the task. 

To record screen plus voice, we recommend using Kazam ([https://launchpad.net/kazam](https://launchpad.net/kazam)), but any other app that allows the same, screen plus voice recording, can work. You will need a microphone. Please, before starting the experiment, make a test recording and check that both the voice and the screen are recorded correctly.

Once you are ready, start recording and when the experiment is complete, please send the video to the supervisor.


## BabiaXR CodeCity Evolution (VR)

You are going to visit a city that represents the JetUML project ([https://github.com/prmr/JetUML](https://github.com/prmr/JetUML)). Each building represents a file in the source code of the system. Each quarter (or district) represents a folder. Quarters can be nested to represent nested folders. On each building, we mapped the values of three software metrics:

- Area: number of functions (num_funs) of the code file. 

- Height: Lines of Code (LOC) per function. 

- Volume: Total lines of code of the code file.

- Color*: Cyclomatic Complexity Number (see [https://en.wikipedia.org/wiki/Cyclomatic_complexity](https://en.wikipedia.org/wiki/Cyclomatic_complexity))

* The color follows a heatmap scale from blue (low) to red (high).

There is a navigation bar that will change the city layout between the JetUML releases. You can click on the bar and the control buttons (play, pause, skip one, forward, backward) to move between the releases, you’ll see the city changing, because the code, layout, and its metrics represented change as well. 



* If you see the pause button it is because automatic evolution is activated and the city will change in seconds, click on it if you want to stop it.
* In addition, if you see the play button it is because the automatic evolution is paused, click to activate it.
* There are two buttons at the beginning and at the end that will determine the direction of the automatic evolution, the darker button determines the direction (from past to present, from present to past) 

Please, take your time to get used to it. When you are ready to start, let the supervisor know.


## Pre-Experiment Requisites

When the supervisor tells you to start recording, please go to the main navigation menu of the Oculus system, click on the share button (the button with the arrow), click on start recording and grant permission to access the microphone.

Once the experiment is complete, proceed again to the main navigation menu of the Oculus system,  then click on the share button (the button with the arrow) and click on stop recording. Let the supervisor know that you finished recording and she/he will extract the video from the glasses.


## Demographics

From now on, for the whole duration of the experiment, you are kindly requested to speak aloud. Please read aloud all the questions and provide all the answers loud and clear.



* How old are you? \

* Job position (e.g., developer, researcher, project manager) \

* Experience level in Programming \

    * None
    * Beginner
    * Knowledgeable
    * Advanced
    * Expert
* Experience level in using an IDE (Pycharm, Eclipse, Visual Studio, etc.) \

    * None
    * Beginner
    * Knowledgeable
    * Advanced
    * Expert
* Programming experience (in years) \

    * Less than 1
    * 1-3
    * 4-6
    * 7-10
    * 10
* How long have you been using an IDE (in years)? \

    * Less than 1
    * 1-3
    * 4-6
    * 7-10
    * 10
* Familiarity with the JetUML system ([https://github.com/prmr/JetUML](https://github.com/prmr/JetUML))? \

    * None
    * Beginner
    * Knowledgeable
    * Advanced
    * Expert
* Experience with Software Visualization \

    * None
    * Beginner
    * Knowledgeable
    * Advanced
    * Expert
* Experience with VR devices (*This is only presented to VR participants) \

    * None
    * Beginner
    * Knowledgeable
    * Advanced
    * Expert


# Tasks

Please read aloud all the questions and provide all the answers loud and clear


## Higher-level questions

T1. Please, notify the supervisor that the task is about to begin and remember to talk aloud.



* Find the test and source directories/packages and observe their evolution along the releases. In which part of the project (directory, package) are the tests located? Do they keep the location along the releases?

When you are done, please comment on the following sentence: The task was difficult (choose one: strongly agree, agree,  don’t know, disagree, strongly disagree). Please notify the supervisor that the task has been completed.



* **Correct answer: Left part of the city are the tests (test and src package), it keeps the same location along all the releases**

T2. Please, notify the supervisor that the task is about to begin and remember to talk aloud.



* Inside the ‘src’ directory, there is a subdirectory called ‘**org’**, When it is included for the first time?

When you are done, please comment on the following sentence: The task was difficult (choose one: strongly agree, agree,  don’t know, disagree, strongly disagree). Please notify the supervisor that the task has been completed.



* **Correct answer: Release 2.0 (2018-05-08)**

T3. Please, notify the supervisor that the task is about to begin and remember to talk aloud.



* When do you find the most substantial change in the software system?

When you are done, please comment on the following sentence: The task was difficult (choose one: strongly agree, agree,  don’t know, disagree, strongly disagree). Please notify the supervisor that the task has been completed.



* **Correct answer: Release 1.2 (2017-11-10) to Release 2.0 (2018-05-08). The names and the structure of the project change.**


## Lower-level questions

T4. Please, notify the supervisor that the task is about to begin and remember to talk aloud.



* Go to the first release of the JetUML project and find the Graph.java class, When was the class removed? Was the directory of the class removed as well?

When you are done, please comment on the following sentence: The task was difficult (choose one: strongly agree, agree,  don’t know, disagree, strongly disagree). Please notify the supervisor that the task has been completed.



* **Correct answer: in the step from Release 2.0 (2018-05-08) to Release 2.1 (2018-06-28). The directory of the class was removed as well, and refactored to the diagram class.**

T5. Please, notify the supervisor that the task is about to begin and remember to talk aloud.



* Go to the latest release and locate the directory ‘**gui/tips’**, When it was created? Does it have a test folder as well? In which release they have been included?

When you are done, please comment on the following sentence: The task was difficult (choose one: strongly agree, agree,  don’t know, disagree, strongly disagree). Please notify the supervisor that the task has been completed.



* **Correct answer: in Release 3.1, the tests are included in the same release.**

T6. Please, notify the supervisor that the task is about to begin and remember to talk aloud.



* Go to the latest release of JetUML, find the file with the most lines of code per function, and the file with the most ccn (complexity). Then go to the first release, find the file with the most lines of code per function, and the file with the most ccn (complexity). Finally, go to the Release 2.4 of JetUML, find the file with the most lines of code per function, and the file with the most ccn (complexity)

When you are done, please comment on the following sentence: The task was difficult (choose one: strongly agree, agree,  don’t know, disagree, strongly disagree). Please notify the supervisor that the task has been completed.



* **Correct answer: **
1. **Latest release: TestPersistenceService.java more lines of code per function, JSONObject with the most ccn.**
2. **First release: TestPersistenceService.java more lines of code per function, GraphPanel.java with the most ccn.**
3. **Rel 2.4: TestPersistenceService.java more lines of code per function, JSONObject with the most ccn.**

T7. Please, notify the supervisor that the task is about to begin and remember to talk aloud.



* Locate the directory ‘src/ca/mcgill/cs/jetuml/diagram’ in the latest release of the project. What is the class with the most lines of code per function of this directory? When this directory has been added?

When you are done, please comment on the following sentence: The task was difficult (choose one: strongly agree, agree,  don’t know, disagree, strongly disagree). Please notify the supervisor that the task has been completed.



* **Correct answer: In the last release the file with the most line of code per function is: Node.java. The directory was added in the Release 2.1**

T8. Please, notify the supervisor that the task is about to begin and remember to talk aloud.



* Go to the Release 2.5, How many source code files does the ‘src/ca/mcgill/cs/jetuml/viewers/nodes’ have?

When you are done, please comment on the following sentence: The task was difficult (choose one: strongly agree, agree,  don’t know, disagree, strongly disagree). Please notify the supervisor that the task has been completed.



* **Correct answer: 15 files**

T9. Please, notify the supervisor that the task is about to begin and remember to talk aloud.



* Go to the Release 2.5 and find the source code file ‘ClassDiagramBuilder.java’, How many functions/methods does it have? Then go to the Release 2.6, How many functions/methods does it have?

When you are done, please comment on the following sentence: The task was difficult (choose one: strongly agree, agree,  don’t know, disagree, strongly disagree). Please notify the supervisor that the task has been completed.



* **Correct answer: In the Release 2.5 the class has 5 methods, and in the Release 2.6 has 15 methods.**


## Second part

So far you are done with the main part of the experiment, we appreciate your participation in this experiment, but we ask you to repeat a set of the tasks that you have done in the other environment:

T6. Please, notify the supervisor that the task is about to begin and remember to talk aloud.



* Go to the latest release of JetUML, find the file with the most lines of code per function, and the file with the most ccn (complexity). Then go to the first release, find the file with the most lines of code per function, and the file with the most ccn (complexity). Finally, go to the Release 2.4 of JetUML, find the file with the most lines of code per function, and the file with the most ccn (complexity)

When you are done, please comment on the following sentence: The task was difficult (choose one: strongly agree, agree,  don’t know, disagree, strongly disagree). Please notify the supervisor that the task has been completed.



* **Correct answer: **
1. **Latest release: TestPersistenceService.java more lines of code per function, JSONObject with the most ccn.**
2. **First release: TestPersistenceService.java more lines of code per function, GraphPanel.java with the most ccn.**
3. **Rel 2.4: TestPersistenceService.java more lines of code per function, JSONObject with the most ccn.**

T8. Please, notify the supervisor that the task is about to begin and remember to talk aloud.



* Go to the Release 2.5, How many source code files does the ‘src/ca/mcgill/cs/jetuml/viewers/nodes’ have?

When you are done, please comment on the following sentence: The task was difficult (choose one: strongly agree, agree,  don’t know, disagree, strongly disagree). Please notify the supervisor that the task has been completed.



* **Correct answer: 15 files**

T9. Please, notify the supervisor that the task is about to begin and remember to talk aloud.



* Go to the Release 2.5 and find the source code file “ClassDiagramBuilder.java”, How many functions/methods does it have? Then go to the Release 2.6, How many functions/methods does it have now?

When you are done, please comment on the following sentence: The task was difficult (choose one: strongly agree, agree,  don’t know, disagree, strongly disagree). Please notify the supervisor that the task has been completed.



* **Correct answer: In the Release 2.5 the class has 5 methods, and in the Release 2.6 has 15 methods.**

Once done, please answer the next questions:

S1. In which environment did you find it easier to complete the tasks? Why?

S2. In which environment has it taken you the longest to complete tasks? Why?

S3. Tell us which parts of each environment are useful to answer the tasks, and tell us which parts make it more difficult to answer the tasks. (Advantages and disadvantages)


## Control questions

C1. Overall, did you find the experiment difficult? (choose one: strongly agree, agree,  don’t know, disagree, strongly disagree) Please explain.

C2. Do you have any suggestions or comments?

C3. In the case you did the on-screen experiment, did you use any other source apart from the web page to analyze and answer the tasks? (i.e. an IDE, the command line, git bash, …)

You have finished the experiment, thanks for participating!
